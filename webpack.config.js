var path = require('path');
var webpack = require('webpack');



module.exports = {
    context: __dirname,
    entry: {
        index: './src/js/index'
    },
    output: {
        path: path.resolve("./bundles/"),
        filename: "[name].js",
        publicPath: "/bundles/"
    },
    plugins: [
    ],
    module: {

        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ["babel?presets[]=es2015,presets[]=stage-0,plugins[]=transform-decorators-legacy"]
            },
            {
                test: /\.css$/,
                loaders: [
                    'style-loader',
                    'css-loader?importLoaders=1',
                    'postcss-loader?sourceMap=inline'
                ]
            }
        ]
    },

    postcss: function(){
        return [require('autoprefixer'), require('precss')];
    },

    resolve: {
        modulesDirectories: ["node_modules", "bower_components"],
        extensions: ["", ".js", ".jsx"]
    }
}
